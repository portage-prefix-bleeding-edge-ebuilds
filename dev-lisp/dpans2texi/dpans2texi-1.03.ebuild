# Distributed under the terms of the GNU General Public License v2

EAPI="prefix"

inherit flag-o-matic

DESCRIPTION="Converts the TeX sources of the draft ANSI Common Lisp standard (dpANS) to the Texinfo format."
HOMEPAGE="http://www.phys.au.dk/~harder/dpans.html"
SRC_URI="http://www.phys.au.dk/~harder/dpans2texi-${PV}.tar.gz
		 http://quimby.gnus.org/circus/cl/dpANS3.tar.gz
		 http://quimby.gnus.org/circus/cl/dpANS3R.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86-macos"
IUSE=""
DEPEND=">=sys-apps/texinfo-4.7
		net-misc/wget
		virtual/emacs"

src_unpack () {
	unpack ${A}
	mv "${WORKDIR}"/dpANS3/*.tex "${S}"
	mv "${WORKDIR}"/dpANS3R/*.tex "${S}"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}
